#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <libqmi-glib.h>
#include <qmi-message.h>
#include <qmi-enums.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

static gchar *xml_file_path;

static GOptionEntry entries[] =
{
    {"file", 'f', 0, G_OPTION_ARG_FILENAME, &xml_file_path, "Kernel dump XML file path", NULL},
    {NULL}
};

size_t convert_hex_string_to_uint8_array(uint8_t *dest, size_t count, const char *src)
{
    size_t i;
    int value;
    for (i = 0; i < count && sscanf(src + i * 2, "%2x", &value) == 1; i++)
        dest[i] = value;
    return i;
}

void remove_space_and_special_characters(char *s)
{
    char *d = s;
    do
    {
        while (*d == ' ' || *d == '\n' || *d == '\t')
            ++d;
    } while ((*s++ = *d++));
}

gchar *decode_qmi_data(int service_id, const gchar *qmi_string)
{
    g_autoptr(GByteArray) qmi = NULL;
    g_autoptr(QmiMessage) message = NULL;
    g_autoptr(GError) error = NULL;
    g_autoptr(GString) string = NULL;
    guint8 *buf = NULL;
    int buf_len = 0;

    string = g_string_new(qmi_string);
    buf_len = string->len / 2;

    buf = g_malloc0(buf_len);
    convert_hex_string_to_uint8_array(buf, buf_len, qmi_string);

    qmi = g_byte_array_sized_new(buf_len);
    g_byte_array_append(qmi, buf, buf_len);

    message = qmi_message_new_from_data(service_id, 0x01, qmi, &error);
    if (!message || error)
    {
        return NULL;
    }

    return qmi_message_get_printable_full(message, NULL, "<<<<<< ");
}

int main(int argc, char **argv)
{
    GError *error = NULL;
    GOptionContext *context;
    long int message_count = 0;

    context = g_option_context_new("- QMI Parse Kernel Dump");
    g_option_context_add_main_entries(context, entries, NULL);
    if (!g_option_context_parse(context, &argc, &argv, &error))
    {
        g_print("option parsing failed: %s\n", error->message);
        exit(1);
    }

    if (!g_file_test(xml_file_path, (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR)))
    {
        g_print("\n\nParsing Failed. File does not exist: %s\n\n", xml_file_path);
        exit(1);
    }

    gchar *file_string = NULL;
    if (!g_file_get_contents(xml_file_path, &file_string, NULL, NULL))
    {
        g_print("\n\nFailed to read file: %s\n\n", xml_file_path);
        exit(1);
    }

    GString *xml_string = g_string_new(file_string);
    xml_string = g_string_prepend(xml_string, "<xml>");
    xml_string = g_string_append(xml_string, "</xml>");

    xmlDoc *doc = xmlParseDoc((xmlChar *)xml_string->str);
    xmlNode *root = xmlDocGetRootElement(doc);
    xmlNode *cur = root->xmlChildrenNode;

    while (cur != NULL)
    {
        if (!(xmlStrcmp(cur->name, (const xmlChar *)"qmi")))
        {
            g_autofree gchar *print = NULL;

            int service_id, msg_type;
            char *svc_id = (char *)xmlGetProp(cur, (const xmlChar *)"svc_id");
            sscanf(svc_id, "%x", &service_id);
            char *type = (char *)xmlGetProp(cur, (const xmlChar *)"type");
            sscanf(type, "%x", &msg_type);
            char *txn_id = (char *)xmlGetProp(cur, (const xmlChar *)"txn_id");
            char *msg_id = (char *)xmlGetProp(cur, (const xmlChar *)"msg_id");
            char *android_process = (char *)xmlGetProp(cur, (const xmlChar *)"android_process");

            g_autofree gchar *qmi_content = (char *)xmlNodeGetContent(cur);
            remove_space_and_special_characters(qmi_content);

            message_count = message_count + 1;
            g_print("x--------------------------------------------------------------------\n");
            g_print("x--------------------------QMI MESSAGE %ld----------------------------\n", message_count);
            g_print("x-----------------Android Process: %s------------------------\n\n", android_process);
            g_print("QMI Service ID     = 0x%s\n", g_ascii_strup(&svc_id[2], -1));
            g_print("QMI Message ID     = 0x%s\n", g_ascii_strup(&msg_id[2], -1));
            switch (msg_type)
            {
            case 0:
                g_print("QMI Message Type   = request\n");
                break;
            case 2:
                g_print("QMI Message Type   = response\n");
                break;
            case 4:
                g_print("QMI Message Type   = indication\n");
                break;
            default:
                g_print("QMI Message Type   = %s\n", type);
                break;
            }
            g_print("QMI Transaction ID = %s\n\n", txn_id);

            print = decode_qmi_data(service_id, qmi_content);
            if (print)
                g_print(">>>>>> Parsed QMI using Libqmi\n%s\n", print);
            else
            {
                g_print("\nFailed to parse QMI using libqmi\n\n");
                GString *qmi_string = g_string_new(qmi_content);

                g_print("QMI String = \n");
                for (int i = 0; i < qmi_string->len; i = i + 2)
                {
                    g_print("%c%c:", qmi_string->str[i], qmi_string->str[i + 1]);
                    if ((i != 0) && (i % 64 == 0))
                        g_print("\n");
                }
                g_print("\n");
            }
        }
        cur = cur->next;
    }

    return 0;
}
